//= include ../bower_components/jquery/dist/jquery.js
//= include ../bower_components/angular/angular.js
//= include ../bower_components/angular-bootstrap/ui-bootstrap.min.js
//= include ../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js
//= include ../bower_components/underscore/underscore.js
//= include ../bower_components/angular-google-maps/dist/angular-google-maps.js
//= include ../bower_components/angular-file-upload/angular-file-upload.js
//= include ../bower_components/angular-gravatar/src/md5.js
//= include ../bower_components/angular-gravatar/build/angular-gravatar.js

angular.module('traflr-dashboard',['ui.bootstrap','google-maps','underscore','ui.gravatar']);

angular.module('underscore', []).factory('_', function() {
    return window._; // assumes underscore has already been loaded on the page
});

angular.module('traflr-dashboard')
.controller('DashboardCtrl',['$scope','traflrInit','$http','$filter',function($scope,traflrInit,$http,$filter){
	//google.maps.visualRefresh = true;
    $scope.user =  'Yaya Turei';
	$scope.username = '@yayaturei_';
    $scope.trips = {};
    $scope.selectedDs = 'trip';

    var getTrips = function(){
        $http({
            method: 'GET',
            url: '/api/feed',
            headers: {'token': token,'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .success(function(data){
            $scope.trips = data;
            console.log(data);
        })
        .error(function(error){
            console.log(error);
        })
    }

    getTrips();

	var token = traflrInit.token;

	$scope.map = {
	    center: {
	        latitude: -7.8032504,
	        longitude: 110.3748449
	    },
	    zoom: 14,
	    draggable: true,
        control: {}
	};

	$scope.postTypes = [
		{
			'name': 'post',
			'icon': 'fa fa-pencil',
			'active': 'true'
		},
		{
			'name': 'photo',
			'icon': 'fa fa-image-o',
			'active': 'false'
		},
		{
			'name': 'video',
			'icon': 'fa fa-video',
			'active': 'false'
		}
	]

	$scope.getInclude = function(param){
		if(param == 'post'){
			return "/assets/partial/dashboard.post.html";
		}else{
			return "/assets/partial/dashboard.trip.html";

		}
	}

	$scope.mapRefresh = false;
	$scope.location = {
	        latitude: -7.77586,
	        longitude: 110.38400000000001
	    };


    $scope.doSearch = function(){
        if($scope.location === ''){
            alert('Directive did not update the location property in parent controller.');
        } else {
            console.log($scope.location);
        }
    };


    $scope.addTrip = {};
    $scope.addTrip.tripTitle = "";

    $scope.getLatitude = function(param){
        var spl = param.split(',');
        return parseFloat(spl[0]);
    }

    $scope.getLongitude = function(param){
        var spl = param.split(',');
        return parseFloat(spl[1]);
    }

    $scope.postTrip = function(){
    	$scope.addTrip.location = $scope.location;
        $scope.addTrip.locationName = subLoc.name;
        var coordinat = ''+subLoc.latitude+','+subLoc.longitude+'';
    	$scope.mapRefresh = true;
    	console.log($scope.addTrip.tripDate);
  		$http({
        	method: 'POST',
        	url: '/api/trip/create',
        	data: 'title='+$scope.addTrip.tripTitle+'&location='+$scope.addTrip.locationName+'&date='+$filter('date')($scope.addTrip.tripDate, 'yyyy-MM-dd')+'&coordinat='+coordinat,
        	headers: {'token': token,'Content-Type': 'application/x-www-form-urlencoded'}
      	}).success(function(data){
            console.log(data);
            if(data.success == 1){
                $scope.$apply($scope.trips.data.unshift(data.data));
            }
            $scope.addTrip.tripTitle = null;
            $scope.addTrip.tripDate = null;
            $scope.addTrip.locationName = null;
            $('#google_places_ac').val('');
            $scope.newTrip =false;
            

      	}).error(function(error){
      		console.log(error);
      	})
    }

    $scope.opened = false;
     $scope.open = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
      }
    $scope.refresh=false;
    $scope.refresh = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.refresh = true;
      }

      $scope.defaultCenter = {
        latitude :35.4437078,
        longitude: 139.6380256
      }

      $scope.refreshMap = function(){
        //console.log($scope.map.control);
        $scope.map.control.refresh();
        //$scope.map.control.getGMap().setZoom(11);
        //return;
      }

      $scope.$on('refreshMaph',function(){
        $scope.map.control.refresh();
        $scope.map.control.getGMap().setZoom(11);
      })

      $scope.toTripNew = function(){
        $scope.selectedDs = 'trip';
        $scope.newTrip = true;
      }

      /*===================================*/
      $scope.posts = {};
      $scope.createPost = function(trip_id){
        $scope.selectedDs = 'post';
        $scope.currentTrip = trip_id;
        $scope.newPost = true;
        getPost();
      }

      $scope.toPost = function(trip_id){
        $scope.selectedDs = 'post';
        $scope.currentTrip = trip_id;
        $scope.newPost = false;
        getPost();
      }

      $scope.postLocation = '';

      $scope.first = {
        latitude : 0,
        longitude: 0
      };

      $scope.poly = {};
      $scope.pol = [];
      $scope.stroke = {
                            color: '#6060FB',
                            weight: 3
                        };

      var getPost = function(){
        $http.get('/api/feed/trip_detail/'+$scope.currentTrip+'/')
        .success(function(data){
            $scope.posts = data;
            $scope.postLocation = {
                latitude:$scope.getLatitude(data.trip.coordinat),
                longitude:$scope.getLongitude(data.trip.coordinat),
                name: data.trip.title
            }
            $scope.poly = data;
              toPath();
              console.log($scope.pol);
              $scope.first = {
                latitude: $scope.getLatitude(data.data[0].coordinat),
                longitude: $scope.getLongitude(data.data[0].coordinat)
              }
            console.log($scope.posts);
        })
        .error(function(error){
            console.log(error);
        })
      }

      var toPath = function(){
        angular.forEach($scope.poly.data, function(path) {
              //if (!todo.done) $scope.todos.push(todo);
              console.log(path);
              var x = path.coordinat.split(',');
              var lt = {
                latitude: x[0],
                longitude: x[1]
              }
              $scope.pol.push(lt);
          });
      }

      $scope.addPost = {};
      $scope.sendPost = function(){
        var coordinat = ''+subLoc.latitude+','+subLoc.longitude+'';
        console.log($scope.addPost.file);
        var fd = new FormData();
        var tanggal = Date.now();
        fd.append('trip_id',$scope.currentTrip);
        fd.append('caption',$scope.addPost.caption);
        fd.append('time',$filter('date')(tanggal, 'yyyy-MM-dd HH:mm:ss'));
        fd.append('coordinat',coordinat);
        fd.append('userfoto',$scope.files);
        console.log(fd);
        $http.post(
            '/api/post/create',fd,
            {
                transformRequest:angular.identity,
                headers: {'token': token,'Content-Type': undefined}
            })
        .success(function(data){
            $scope.posts.data.unshift(data.data);
            console.log(data);
            $scope.addPost.caption = null;
            fd = null;
            $scope.newPost = false;
        })
        .error(function(error){
            console.log(error);
        })
      }

      $scope.fileNameChanged = function(param){
        $scope.filo = param;
      }

      $scope.selectFile = function()
       {
            $("#file").click();
       }

       $scope.files = {};
      $scope.setFiles = function(element) {
        $scope.$apply(function(scope) {
          //console.log('files:', element.files);
          // Turn the FileList object into an Array
            $scope.files = element.files[0];
            console.log($scope.files);
            /*for (var i = 0; i < element.files.length; i++) {
              scope.files.push(element.files[i])
            }*/
          
          });
        };


      /*$scope.onFileSelect = function($files) {
        //$files: an array of files selected, each file has name, size, and type.
        for (var i = 0; i < $files.length; i++) {
          var file = $files[i];
          $scope.upload = $upload.upload({
            url: '/api/post/create', //upload.php script, node.js route, or servlet url
            method: 'POST',
            // headers: {'header-key': 'header-value'},
            // withCredentials: true,
            data: {myObj: $scope.myModelObj},
            file: file, // or list of files: $files for html5 only
            /* set the file formData name ('Content-Desposition'). Default is 'file' */
            //fileFormDataName: myFile, //or a list of names for multiple files (html5).
            /* customize how data is added to formData. See #40#issuecomment-28612000 for sample code */
            //formDataAppender: function(formData, key, val){}
          /*}).progress(function(evt) {
            console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
          }).success(function(data, status, headers, config) {
            // file is uploaded successfully
            console.log(data);
          });*/
          //.error(...)
          //.then(success, error, progress); 
          //.xhr(function(xhr){xhr.upload.addEventListener(...)})// access and attach any event listener to XMLHttpRequest.
        //}
        /* alternative way of uploading, send the file binary with the file's content-type.
           Could be used to upload files to CouchDB, imgur, etc... html5 FileReader is needed. 
           It could also be used to monitor the progress of a normal http post/put request with large data*/
        // $scope.upload = $upload.http({...})  see 88#issuecomment-31366487 for sample code.
      //};*/

}])
.directive('googlePlaces',[function(){
                return {
                    restrict:'E',
                    replace:true,
                    // transclude:true,
                    scope: {
                        location:'=',
                        refreshMap: '&'
                    },
                    template: '<input id="google_places_ac" name="google_places_ac" type="text" class="form-control" />',
                    link: function($scope, elm, attrs){
                        var autocomplete = new google.maps.places.Autocomplete($("#google_places_ac")[0], {});
                        google.maps.event.addListener(autocomplete, 'place_changed', function() {
                            var place = autocomplete.getPlace();
                            $scope.location = {
                                latitude  :place.geometry.location.lat(),
                                longitude :place.geometry.location.lng(),
                                viewport  :place.geometry.viewport
                            }
                            subLoc = {
                                latitude  :place.geometry.location.lat(),
                                longitude :place.geometry.location.lng(),
                                name :place.name
                            }
                            $scope.refreshMap();
                            $scope.$apply();
                        });

                    }
                }
            }]);