//= include ../bower_components/jquery/dist/jquery.js
//= include ../bower_components/angular/angular.min.js
//= include ../bower_components/angular-bootstrap/ui-bootstrap.min.js
//= include ../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js
//= include ../bower_components/underscore/underscore.js
//= include ../bower_components/angular-google-maps/dist/angular-google-maps.js
//= include ../bower_components/angular-gravatar/src/md5.js
//= include ../bower_components/angular-gravatar/build/angular-gravatar.js

angular.module('traflr',['ui.bootstrap','google-maps','underscore','ui.gravatar']);

angular.module('underscore', []).factory('_', function() {
    return window._; // assumes underscore has already been loaded on the page
});

angular.module('traflr')
.controller('BlogCtrl',['$scope','usInit','$http',function($scope,usInit,$http){
	$scope.user =  'Yaya Turei';
	$scope.username = '@yayaturei_';
	var userId = usInit.id;

	$scope.trips = {};

	var getTrip = function(){
		$http.get('/api/feed/user/'+userId)
		.success(function(data){
			$scope.trips = data;
			console.log(data);
		})
		.error(function(error){
			console.log(error);
		});
	};

	getTrip();

	$scope.getLatitude = function(param){
		//console.log(param);
        var spl = param.split(',');
        return parseFloat(spl[0]);
    };

    $scope.getLongitude = function(param){
        var spl = param.split(',');
        return parseFloat(spl[1]);
    };

    $scope.poly = {
    	path:[
    		{
    			latitude:-7.719582,
    			longitude:110.565248
    		},
	        {
	            latitude: -7.71959,
	            longitude: 110.56525
	        },
	        {
	            latitude: 32,
	            longitude: -89
	        },
	        {
	            latitude: 39,
	            longitude: -122
	        },
	        {
	            latitude: 62,
	            longitude: -95
	        }
	    ],
	    stroke: {
                color: '#6060FB',
                weight: 3
            }
    }
}]);