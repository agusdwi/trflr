//= include ../bower_components/jquery/dist/jquery.js
//= include ../bower_components/angular/angular.min.js
//= include ../bower_components/angular-bootstrap/ui-bootstrap.min.js
//= include ../bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js

angular.module('traflr-home',['ui.bootstrap']);

angular.module('traflr-home')
.controller('MainCtrl',['$scope',function($scope){
	$scope.user =  'Yaya Turei';
	$scope.username = '@yayaturei_';

	$scope.map = {
	    center: {
	        latitude: -7.8032504,
	        longitude: 110.3748449
	    },
	    zoom: 13,
	    draggable: true
	};
}])
.controller('ModalInstanceCtrl',['$scope','$modalInstance','items','$http',function($scope, $modalInstance,$http,items){
  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  $scope.login = function(){
    console.log('montana');
    $http.post('/auth/login',{
      'email': $scope.email,
      'password': $scope.password
    },{'token': token,'Content-Type': 'application/x-www-form-urlencoded'})
    .success(function(data){
      console.log(data);
    })
    .error(function(error){
      console.log(error);
    })
  }
}])
.controller('ModalDemoCtrl',['$scope', '$modal', '$log','$http',function($scope, $modal, $log,$http){
 $scope.items = ['item1', 'item2', 'item3'];

  $scope.open = function (size) {

    var modalInstance = $modal.open({
      templateUrl: 'loginModal.html',
      controller: 'ModalInstanceCtrl',
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };

  
}]);
