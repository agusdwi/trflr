<?
class Post_model extends CI_Model {

	var $id;
	var $error = '';

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
	}

	function insert($token){
		$this->id = $this->user_model->authToken($token);
		$trip_id  = $this->input->post('trip_id');

		if($this->validate($this->id,$trip_id)){
			$this->form_validation->set_rules('trip_id', 'trip_id', 'required');
			$this->form_validation->set_rules('time', 'time', 'required');
			$this->form_validation->set_rules('coordinat', 'coordinat', 'required');
			$this->form_validation->set_rules('caption', 'caption', 'required');
			$this->form_validation->set_error_delimiters('','');

			if ($this->form_validation->run() == FALSE){
				return (object) array('success'=>false,'message'=> validation_errors());
			}else{
				$data = $this->input->post();

				$this->process_upload($data);
				
				$this->db->insert('post',$data);

				$this->error = ($this->error != '')? 'But '.strip_tags($this->error) : '' ;
				$ins_id = $this->db->insert_id();
				$post = $this->db->get_where('post',array('id'=>$ins_id))->row();

				return (object) array('success'=>true,'message'=>'posting successfull.'.$this->error,'data'=>$post);
			}
		}else{
			return (object) array('success'=>false,'message'=>'unauthorized access');
		}
	}

	function get($token,$id){
		$this->id = $this->user_model->authToken($token);

		$ds = $this->db->get_where('post',array('id'=>$id));
		if($ds->num_rows() == 1){
			$ds = $ds->row();
			if($this->validate($this->id,$ds->trip_id)){
				return (object) array('success'=>true,'data'=> $ds);
			}else{
				return (object) array('success'=>false,'message'=>'unauthorized access');
			}
		}else return (object) array('success'=>false,'message'=> 'not found');	
	}

	function edit($token,$id){
		$this->id 	= $this->user_model->authToken($token);
		$ds 		= $this->db->get_where('post',array('id'=>$id));

		if($ds->num_rows() == 1){
			$ds = $ds->row();

			if($this->validate($this->id,$ds->trip_id)){
				$this->form_validation->set_rules('time', 'time', 'required');
				$this->form_validation->set_rules('coordinat', 'coordinat', 'required');
				$this->form_validation->set_rules('caption', 'caption', 'required');
				$this->form_validation->set_error_delimiters('','');

				if ($this->form_validation->run() == FALSE){
					return (object) array('success'=>false,'message'=> validation_errors());
				}else{
					$data = $this->input->post();

					$this->process_upload($data);

					$this->db->where('id', $id);
					$this->db->update('post', $data);

					$this->error = ($this->error != '')? ' but '.strip_tags($this->error) : '' ;

					return (object) array('success'=>true,'message'=> 'post has been update'.$this->error);
				}
			}else return (object) array('success'=>false,'message'=>'unauthorized access');

		}else return (object) array('success'=>false,'message'=> 'not found');
	}
	
	function delete($token,$id){
		$this->id 	= $this->user_model->authToken($token);
		$ds 		= $this->db->get_where('post',array('id'=>$id));

		if($ds->num_rows() == 1){
			$ds = $ds->row();
			if($this->validate($this->id,$ds->trip_id)){
				$this->db->delete('post', array('id' => $id)); 
				return (object) array('success'=>true,'message'=> 'post has been deleted');
			}else return (object) array('success'=>false,'message'=>'unauthorized access');
		}else return (object) array('success'=>false,'message'=> 'not found');
	}

	function validate($id,$trip_id){
		if ($this->db->get_where('trip',array('id'=>$trip_id,'user_id'=>$id))->num_rows() == 1) {
			return true;
		}else return false;
	}

	function process_upload(&$data){
		$foto = $this->upload_foto();
		if($foto->success){
			$data['photo'] = $foto->data;
			$data['video'] = '';
		}else{
			if ($foto->message != 'no-file') {
				$this->error .= $foto->message;
			}
		}

		$video = $this->upload_video();
		if($video->success){
			$data['video'] = $video->data;
			$data['photo'] = '';
		}else{
			if ($video->message != 'no-file') {
				$this->error .= $video->message;
			}
		}
	}

	function upload_foto(){
		if(isset($_FILES['userfoto'])){
			$_FILES['userfoto']['name']	= strtolower($_FILES['userfoto']['name']);
			if(!empty($_FILES['userfoto']['name'])){
				$config['upload_path']		= 'media/foto/';
				$config['allowed_types']	= 'gif|jpg|png';
				$config['max_size']			= '10000';
				$config['max_width']		= '5000';
				$config['max_height']		= '5000';
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('userfoto')){
					return (object)array('success'=>false,'message'=>$this->upload->display_errors());
				}else{
					$a = $this->upload->data();
					return (object)array('success'=>true,'data'=>$a['file_name']);
				}
			}else{
				return (object)array('success'=>false,'message'=>'no-file');
			}
		}else return (object)array('success'=>false,'message'=>'no-file');
	}

	function upload_video(){
		if(isset($_FILES['uservideo'])){
			$_FILES['uservideo']['name']= strtolower($_FILES['uservideo']['name']);
			if(!empty($_FILES['uservideo']['name'])){
				$config['upload_path']		= 'media/video/';
				$config['allowed_types']	= '*';
				$config['max_size']			= '10000000000';
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('uservideo')){
					return (object)array('success'=>false,'message'=>$this->upload->display_errors());
				}else{
					$a = $this->upload->data();
					return (object)array('success'=>true,'data'=>$a['file_name']);
				}
			}else{
				return (object)array('success'=>false,'message'=>'no-file');
			}
		}else return (object)array('success'=>false,'message'=>'no-file');
	}
}
?>