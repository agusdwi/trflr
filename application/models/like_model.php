<?
class Like_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->per_page = 10;
	}

	// start like trip

	function insert_trip($user_id,$trip_id){
		$trip = $this->db->get_where('trip',array('id'=>$trip_id));
		if ($trip->num_rows() == 1) {
			
			$trip_like = $this->db->get_where('like_trip',array('user_id'=>$user_id,'trip_id'=>$trip_id));

			if($trip_like->num_rows() == 1){
				return (object) array('success'=>false,'message'=>'already like');	
			}else{
				$data['user_id'] = $user_id;
				$data['trip_id'] = $trip_id;
				$this->db->insert('like_trip',$data);
				return (object) array('success'=>true,'message'=>'like successfull');
			}
		}else{
			return (object) array('success'=>false,'message'=>'not found');
		}
	}

	function get_trip($id,$page){
		$page *= $this->per_page;
		$ds = $this->db
				->select('like_trip.*,user.email,user.username,user.name,user.picture')
				->limit($this->per_page,$page)
				->order_by('id','desc')
				->join('user','like_trip.user_id = user.id')
				->get_where('like_trip',array('trip_id'=>$id));
		return (object)array('total'=>$ds->num_rows(),'data'=>$ds->result());
	}

	function delete_trip($id,$trip_id){
		$trip = $this->db->get_where('trip',array('id'=>$trip_id));
		if ($trip->num_rows() == 1) {
			$trip_like = $this->db->get_where('like_trip',array('user_id'=>$id,'trip_id'=>$trip_id));
			if($trip_like->num_rows() == 1){
				$delete = $this->db->delete('like_trip', array('user_id'=>$id,'trip_id'=>$trip_id)); 
				if($this->db->affected_rows() > 0){
					return (object) array('success'=>true,'message'=>'delete successfull');
				}else return (object) array('success'=>false,'message'=>'not found');		
			}else{
				return (object) array('success'=>false,'message'=>'not yet like');
			}
		}else{
			return (object) array('success'=>false,'message'=>'not found');
		}
	}

	// start like post

	function insert_post($user_id,$post_id){
		$trip = $this->db->get_where('post',array('id'=>$post_id));
		if ($trip->num_rows() == 1) {
			
			$post_like = $this->db->get_where('like_post',array('user_id'=>$user_id,'post_id'=>$post_id));

			if($post_like->num_rows() == 1){
				return (object) array('success'=>false,'message'=>'already like');	
			}else{
				$data['user_id'] = $user_id;
				$data['post_id'] = $post_id;
				$this->db->insert('like_post',$data);
				return (object) array('success'=>true,'message'=>'like successfull');
			}
		}else{
			return (object) array('success'=>false,'message'=>'not found');
		}
	}

	function get_post($id,$page){
		$page *= $this->per_page;
		$ds = $this->db
				->select('like_post.*,user.email,user.username,user.name,user.picture')
				->limit($this->per_page,$page)
				->order_by('id','desc')
				->join('user','like_post.user_id = user.id')
				->get_where('like_post',array('post_id'=>$id));
		return (object)array('total'=>$ds->num_rows(),'data'=>$ds->result());
	}

	function delete_post($id,$post_id){
		$trip = $this->db->get_where('post',array('id'=>$post_id));
		if ($trip->num_rows() == 1) {
			$post_like = $this->db->get_where('like_post',array('user_id'=>$id,'post_id'=>$post_id));
			if($post_like->num_rows() == 1){
				$delete = $this->db->delete('like_post', array('user_id'=>$id,'post_id'=>$post_id)); 
				if($this->db->affected_rows() > 0){
					return (object) array('success'=>true,'message'=>'unlike successfull');
				}else return (object) array('success'=>false,'message'=>'not found');		
			}else{
				return (object) array('success'=>false,'message'=>'not yet like');
			}
		}else{
			return (object) array('success'=>false,'message'=>'not found');
		}
	}
}