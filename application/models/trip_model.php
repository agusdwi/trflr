<?
class Trip_model extends CI_Model {

	var $id;

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
	}

	function insert($token){
		$this->id = $this->user_model->authToken($token);

		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('location', 'location', 'required');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() == FALSE){
			return (object) array('success'=>false,'message'=> validation_errors() );
		}else{
			$data = $this->input->post();
			$data['user_id'] = $this->id;
			$this->db->insert('trip',$data);
			$ins_id = $this->db->insert_id();
			$trip = $this->db->get_where('trip',array('id'=>$ins_id))->row();
			return (object) array('success'=>true,'message'=>'register successfull','data'=>$trip);
		}
	}

	function get($token,$id){
		$this->id = $this->user_model->authToken($token);
		$ds = $this->db->get_where('trip',array('id'=>$id));

		if($ds->num_rows() == 1){
			$ds = $ds->row();
			if($ds->user_id == $this->id){
				return (object) array('success'=>true,'data'=> $ds);
			}else return (object) array('success'=>false,'message'=> 'not found');
		}else return (object) array('success'=>false,'message'=> 'not found');
	}

	function edit($token,$id){
		$this->id = $this->user_model->authToken($token);
		$ds = $this->db->get_where('trip',array('id'=>$id));

		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('date', 'date', 'required');
		$this->form_validation->set_rules('location', 'location', 'required');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() == FALSE){
			return (object) array('success'=>false,'message'=> validation_errors() );
		}else{
			if($ds->num_rows() == 1){
				$ds = $ds->row();
				if($ds->user_id == $this->id){
					$data = $this->input->post();
					$this->db->where('id', $id);
					$this->db->update('trip', $data); 
					return (object) array('success'=>true,'message'=> 'trip has been update');
				}else return (object) array('success'=>false,'message'=> 'not found');
			}else return (object) array('success'=>false,'message'=> 'not found');
		}
	}
	
	function delete($token,$id){
		$this->id = $this->user_model->authToken($token);
		$ds = $this->db->get_where('trip',array('id'=>$id));		

		if($ds->num_rows() == 1){
			$ds = $ds->row();
			if($ds->user_id == $this->id){
				$this->db->delete('trip', array('id' => $id)); 
				return (object) array('success'=>true,'message'=> 'trip has been deleted');
			}else return (object) array('success'=>false,'message'=> 'not found');
		}else return (object) array('success'=>false,'message'=> 'not found');
	}
}
?>