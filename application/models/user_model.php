<?
class User_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
	}

	public function authToken($token){
		$ds = $this->db->get_where('user',array('token'=>$token));
		if ($ds->num_rows() != 1) {
			json_out(array('success'=>0,'message'=>'wrong token'));	
			die();
		}else{
			$ds = $ds->row();
			return $ds->id;
		}
	}

	function register(){
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha|min_length[5]|max_length[12]|is_unique[user.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() == FALSE){
			return (object) array('success'=>false,'message'=> validation_errors() );
		}else{
			$data = $this->input->post();
			$data['password'] = $this->encrypt_password($data['password']);
			$this->db->insert('user',$data);
			return (object) array('success'=>true,'message'=>'register successfull');
		}
	}

	function auth(){
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() == FALSE){
			return (object) array('success'=>false,'message'=> validation_errors() );
		}else{
			$this->db->where('email =', $this->input->post('email'));
			$this->db->or_where('username =', $this->input->post('email'));
			$user 		= $this->db->get('user');


			if ($user->num_rows() == 1) {
				$user = $user->row();
				if($user->password == $this->encrypt_password($this->input->post('password'))){
					
					$token = generate_token();
					$data = array('token' => $token);
					$this->db->where('id', $user->id);
					$this->db->update('user', $data); 

					return (object) array('success'=>true,'message'=>'login success','token'=>$token, 'user'=>$user);
				}else{
					return (object) array('success'=>false,'message'=>'login failed, wrong username / password');
				}
			}else{
				return (object) array('success'=>false,'message'=>'login failed, wrong username / password');
			}
		}
	}

	function encrypt_password($p){
		return md5(encryptkey().$p);
	}

	function generate_forgot_email(){
		$email = $this->db->get_where('user',array('email'=>$this->input->post('email')));
		if ($email->num_rows() == 1) {
			$email = $email->row();
			$data = array('hash_forgot' => generate_token());
			$this->db->where('id', $email->id);
			$this->db->update('user', $data); 
			// todo email
			return (object) array('success'=>true,'message'=>'please check your email, and follow links to update password');
		}else{
			return (object) array('success'=>false,'message'=>'email not found');
		}
	}

	function set_new_password($hash_forgot){
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_error_delimiters('','');

		if ($this->form_validation->run() == FALSE){
			return (object) array('success'=>false,'message'=> validation_errors() );			
		}else{
			$valid = $this->db->get_where('user',array('hash_forgot'=>$hash_forgot));
			if ($valid->num_rows() == 1) {
				$valid = $valid->row();
				$data = array(
					'password' => $this->encrypt_password($this->input->post('password'))
					);
				$this->db->where('id', $valid->id);
				$this->db->update('user', $data); 
				return (object) array('success'=>true,'message'=>'password berhasil di update');
			}else return (object) array('success'=>false,'message'=>'link salah, silahlan request forgot password lagi');
			
		}
	}

	function cekUsername($username){
		$uname = $this->db->get_where('user',array('username'=>$username));
		if ($uname->num_rows() == 1) {
			// todo email
			$uname_id = $uname->row()->id;
			return (object) array('success'=>true,'data'=> array('id'=>$uname->row()->id,'email'=>$uname->row()->email,'username'=>$uname->row()->username));
		}else{
			return (object) array('success'=>false,'message'=>'username not valid');
		}
	}
}