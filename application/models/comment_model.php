<?
class Comment_model extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->per_page = 10;
	}

	// start comment trip

	function insert_trip($user_id,$trip_id){
		$trip = $this->db->get_where('trip',array('id'=>$trip_id));
		if ($trip->num_rows() == 1) {
			$this->form_validation->set_rules('comment', 'comment', 'required|max_length[200]');	
			$this->form_validation->set_error_delimiters('','');

			if ($this->form_validation->run() == FALSE){
				return (object) array('success'=>false,'message'=> validation_errors() );
			}else{
				$data = $this->input->post();
				$data['user_id'] = $user_id;
				$data['trip_id'] = $trip_id;
				$this->db->insert('comment_trip',$data);
				return (object) array('success'=>true,'message'=>'comment trip successfull');
			}

		}else{
			return (object) array('success'=>false,'message'=>'not found');
		}
	}

	function get_trip($id,$page){
		$page *= $this->per_page;
		$ds = $this->db
				->select('comment_trip.*,user.email,user.username,user.name,user.picture')
				->limit($this->per_page,$page)
				->order_by('id','desc')
				->join('user','comment_trip.user_id = user.id')
				->get_where('comment_trip',array('trip_id'=>$id));
		return (object)array('total'=>$ds->num_rows(),'data'=>$ds->result());
	}

	function delete_trip($id,$comment_id){
		$delete = $this->db->delete('comment_trip', array('id' => $comment_id,'user_id'=>$id)); 
		if($this->db->affected_rows() > 0){
			return (object) array('success'=>true,'message'=>'delete successfull');
		}else return (object) array('success'=>false,'message'=>'not found');
	}

	// start comment post

	function insert_post($user_id,$post_id){
		$post = $this->db->get_where('post',array('id'=>$post_id));
		if ($post->num_rows() == 1) {
			$this->form_validation->set_rules('comment', 'comment', 'required|max_length[200]');	
			$this->form_validation->set_error_delimiters('','');

			if ($this->form_validation->run() == FALSE){
				return (object) array('success'=>false,'message'=> validation_errors() );
			}else{
				$data = $this->input->post();
				$data['user_id'] = $user_id;
				$data['post_id'] = $post_id;
				$this->db->insert('comment_post',$data);
				return (object) array('success'=>true,'message'=>'comment trip successfull');
			}

		}else{
			return (object) array('success'=>false,'message'=>'not found');
		}		
	}

	function get_post($id,$page){
		$page *= $this->per_page;
		$ds = $this->db
				->select('comment_post.*,user.email,user.username,user.name,user.picture')
				->limit($this->per_page,$page)
				->order_by('id','desc')
				->join('user','comment_post.user_id = user.id')
				->get_where('comment_post',array('post_id'=>$id));
		return (object)array('total'=>$ds->num_rows(),'data'=>$ds->result());
	}

	function delete_post($id,$comment_id){
		$delete = $this->db->delete('comment_post', array('id' => $comment_id,'user_id'=>$id)); 
		if($this->db->affected_rows() > 0){
			return (object) array('success'=>true,'message'=>'delete successfull');
		}else return (object) array('success'=>false,'message'=>'not found');
	}
}
