<?
class Feed_model extends CI_Model {
	
	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		$this->per_page = 10;
	}

	function get($page){
		$page *= $this->per_page;
		$ds = $this->db
				->select('trip.*,user.email,user.username,user.name,user.picture')
				->limit($this->per_page,$page)
				->order_by('id','desc')
				->join('user','trip.user_id = user.id')
				->get('trip');
		$result = array();
		foreach ($ds->result() as $key) {
			$pre_result = $key;
			$post = $this->db->get_where('post',array('trip_id'=>$key->id),1);
			$pre_result->feature_picture = ($post->num_rows() == 1)? $post->row()->photo : "" ;
			$result[] = $pre_result;
		}
		return (object)array('total'=>$ds->num_rows(),'data'=>$ds->result());
	}

	function get_user_trip($id,$page){
		$page *= $this->per_page;
		$user = $this->db->get_where('user',array('id'=>$id))->row();
		$user->trip 		= $this->db->get_where('trip',array('user_id'=>$id))->num_rows();
		$user->user_miles 	= number_format($this->get_distance_user($id),2);

		$ds = $this->db
				->select('trip.*,user.email,user.username,user.name,user.picture')
				->limit($this->per_page,$page)
				->order_by('id','desc')
				->join('user','trip.user_id = user.id')
				->get_where('trip',array('user_id'=>$id));
		return (object)array('total'=>$ds->num_rows(),'data'=>$ds->result(),'user'=>$user);		
	}

	function get_trip_detail($id,$page){
		$page *= $this->per_page;
		$trip = $this->db
				->select('trip.*,user.email,user.username,user.name,user.picture')
				->order_by('id','desc')
				->join('user','trip.user_id = user.id')
				->get_where('trip',array('trip.id'=>$id));

		if($trip->num_rows() == 1){
			$trip = $trip->row();
			$post = $this->db
						->order_by('id','desc')
						->limit($this->per_page,$page)
						->get_where('post',array('trip_id'=>$id));
			$trip->total_miles 	= number_format($this->get_distance($post),2);
			$trip->user_miles 	= number_format($this->get_distance_user($trip->user_id),2);
			$trip->user_trip 	= $this->db->get_where('trip',array('user_id'=>$trip->user_id))->num_rows();
			return (object)array('trip'=>$trip,'total'=>$post->num_rows(),'data'=>$post->result());		
		}else{
			return (object)array('trip'=>array(),'total'=>0);		
		}
	}

	function get_all_trip_detail_user($id,$page){
		$trip = $this->db->select('*')
						 ->from('post as p')
						 ->join('trip as t', 'p.trip_id = t.id')
						 ->get_where('trip tr',array('tr.user_id'=>$id));
		return (object)array('total'=>$trip->num_rows(),'data'=>$trip->result());
	}

	function get_distance($p){
		$d = 0;
		$r = $p->result();
		
		$i=0;
		foreach ($p->result() as $key) {
			$i++;
			if(isset($r[$i])){
				$l1 = explode(",", $key->coordinat);
				$l2 = explode(",", $r[$i]->coordinat);
				$d += $this->distance($l1[0],$l1[1],$l2[0],$l2[1],'K');
			}
		}
		return $d;
	}

	function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);

		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

	function get_distance_user($id){
		$trip = $this->db->get_where('trip',array('user_id'=>$id));
		$tot = 0;

		foreach ($trip->result() as $key) {
			$post = $this->db->get_where('post',array('trip_id'=>$key->id));
			$tot += $this->get_distance($post);
		}

		return $tot;
	}
}