<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('debug_array')) {

    function debug_array($a = array(), $b = false) {//format abjad-abjad atau abjad-angka
        echo "<pre>";
        print_r($a);
        echo "</pre>";
        if (!$b)
            die();
        return 0;
    }

}

//time format
if (!function_exists('format_date_time')) {

    function format_date_time($i, $complete = true) {
		if(($i == '0000-00-00') || empty($i)){
			return '-';
		}else{
			$a = explode(" ", $i);
	        $d = explode("-", $a[0]);
	        if ($complete) {
	            $t = explode(":", $a[1]);
	            return "$d[2]-$d[1]-$d[0] $t[0]:$t[1]";
	        }else
	            return "$d[2]-$d[1]-$d[0]";
		}
    }
}

function date_to_sql($date){
	$d = explode("-", $date);
	return "$d[2]-$d[1]-$d[0]";
}

function indo_day($d){
	switch ($d) {
		case 'Sun':return 'Minggu';break;
		case 'Mon':return 'Senin';break;
		case 'Tue':return 'Selasa';break;
		case 'Wed':return 'Rabu';break;
		case 'Thu':return 'Kamis';break;
		case 'Fri':return 'Jumat';break;
		case 'Sat':return 'Sabtu';break;
	}
}

function indo_day_int($d){
	switch ($d) {
		case 0:return 'minggu';break;
		case 1:return 'senin';break;
		case 2:return 'selasa';break;
		case 3:return 'rabu';break;
		case 4:return 'kamis';break;
		case 5:return 'jumat';break;
		case 6:return 'sabtu';break;
	}
}

//time format
if (!function_exists('pretty_date')) {

    function pretty_date($i,$complete = true) {
		if(($i == '0000-00-00') || empty($i)){
			return '-';
		}else{
			$date = new DateTime($i);
			$day = indo_day($date->format('D'));
		
			$a = explode(" ", $i);
	        $d = explode("-", $a[0]);
			if($complete)
				$j = explode(":", $a[1]);
			$bln = '';
			switch (intval($d[1])) {
				case 1: $bln = 'Januari';break;
				case 2: $bln = 'Februari';break;
				case 3: $bln = 'Maret';break;
				case 4: $bln = 'April';break;
				case 5: $bln = 'Mei';break;
				case 6: $bln = 'Juni';break;
				case 7: $bln = 'Juli';break;
				case 8: $bln = 'Agustus';break;
				case 9: $bln = 'September';break;
				case 10: $bln = 'Oktober';break;
				case 11: $bln = 'November';break;
				case 12: $bln = 'Desember';break;
			}
			if($complete)
				return "$day, $d[2] $bln $d[0] $j[0]:$j[1]";
			else return "$day, $d[2] $bln $d[0]";
		}
    }
}

function smart_trim($text, $max_len, $trim_middle = false, $trim_chars = '...')
{
	$text = strip_tags($text); 
	$text = trim($text);

	if (strlen($text) < $max_len) {

		return $text;

	} elseif ($trim_middle) {

		$hasSpace = strpos($text, ' ');
		if (!$hasSpace) {
			/**
			 * The entire string is one word. Just take a piece of the
			 * beginning and a piece of the end.
			 */
			$first_half = substr($text, 0, $max_len / 2);
			$last_half = substr($text, -($max_len - strlen($first_half)));
		} else {
			/**
			 * Get last half first as it makes it more likely for the first
			 * half to be of greater length. This is done because usually the
			 * first half of a string is more recognizable. The last half can
			 * be at most half of the maximum length and is potentially
			 * shorter (only the last word).
			 */
			$last_half = substr($text, -($max_len / 2));
			$last_half = trim($last_half);
			$last_space = strrpos($last_half, ' ');
			if (!($last_space === false)) {
				$last_half = substr($last_half, $last_space + 1);
			}
			$first_half = substr($text, 0, $max_len - strlen($last_half));
			$first_half = trim($first_half);
			if (substr($text, $max_len - strlen($last_half), 1) == ' ') {
				/**
				 * The first half of the string was chopped at a space.
				 */
				$first_space = $max_len - strlen($last_half);
			} else {
				$first_space = strrpos($first_half, ' ');
			}
			if (!($first_space === false)) {
				$first_half = substr($text, 0, $first_space);
			}
		}

		return $first_half.$trim_chars.$last_half;

	} else {

		$trimmed_text = substr($text, 0, $max_len);
		$trimmed_text = trim($trimmed_text);
		if (substr($text, $max_len, 1) == ' ') {
			/**
			 * The string was chopped at a space.
			 */
			$last_space = $max_len;
		} else {
			/**
			 * In PHP5, we can use 'offset' here -Mike
			 */
			$last_space = strrpos($trimmed_text, ' ');
		}
		if (!($last_space === false)) {
			$trimmed_text = substr($trimmed_text, 0, $last_space);
		}
		return remove_trailing_punctuation($trimmed_text).$trim_chars;

	}

}
function remove_trailing_punctuation($text)
{
	return preg_replace("'[^a-zA-Z_0-9]+$'s", '', $text);
}

function is_post(){
	$CI =& get_instance();
	return ($CI->input->server('REQUEST_METHOD') == "POST");
}

function date_now($complete = true){
	if($complete){
		return DATE('Y-m-d h:i:s');
	}else{
		return DATE('Y-m-d');
	}
}

function json_out($d){
	$a = json_encode($d);
	echo str_replace(array('\n\r','\n','\r','\t'),'',$a);
}

function encryptkey()
{
	$CI =& get_instance();
	return $CI->config->item('encryption_key');
}

function generate_token(){
	list($usec, $sec) = explode(" ",microtime());
	$time = ((float)$usec + (float)$sec);
	$token = base64_encode($time);
	return $token;
}