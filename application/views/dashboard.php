<!DOCTYPE html>
<html ng-app="traflr-dashboard">
<head>
	<title>Dashboard | Traflr</title>
	<link rel="stylesheet" type="text/css" href="/assets/dashboard.css">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
</head>
<body ng-controller="DashboardCtrl">
  <nav class="navbar navbar-default navbar-static-top trflr-navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?echo base_url()?>"><span class="trflr-brand-t">T</span>raflr</a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
        <ul class="nav navbar-nav">
          <li><a href="#" ng-click="selectedDs='trip'">My Trips</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li><button class="btn btn-danger navbar-btn new-trip-btn" ng-show="selectedDs=='post'" ng-click="toTripNew();refreshMap()"><i class="fa fa-plane"></i> New Trip</button></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?echo $this->session->userdata('username');?> <b class="caret"></b></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#" style="color:white">Edit Profile</a></li>
              <li class="devider"></li>
              <li><a href="<?echo base_url('auth/logout') ?>" style="color:white">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container">
  	<div class="row">
  	<div class="col-xs-3">
      <div class="row">
        <div class="col-xs-12">
          <div class="jumbotron trflr-profile-side">
            <div class="row trflr-profife-wrapper">
              <div class="col-xs-4 trflr-avatar-wrapper">
                <a href="#" class="trflr-profile-avatar">
                  <img gravatar-src="'<?echo $this->session->userdata('email');?>'" gravatar-size="100">
                </a>
              </div>
              <div class="col-xs-6 trflr-name-wrapper">
                <h5 class="trflr-profile-name"><a href="#"><?echo '@'.$this->session->userdata('username');?></a></h5>
                <!--a href="#" class="trflr-profile-username"><?echo '@'.$this->session->userdata('username');?></a-->
              </div>
            </div>
            <div class="jumbotron-photo"><img src="/assets/images/blur-background12.jpg" /></div>
            <div class="jumbotron-contents">
              <div class="row">
                <div class="col-xs-6 trflr-label-user"><i class="fa fa-globe"></i> 8 trips</div>
                <div class="col-xs-6 trflr-label-user"><i class="fa fa-exchange"></i> 530 km</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
           <div class="list-group">
              <a href="#" class="list-group-item active">Cras justo odio</a>
              <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
              <a href="#" class="list-group-item">Morbi leo risus</a>
              <a href="#" class="list-group-item">Porta ac consectetur ac</a>
              <a href="#" class="list-group-item">Vestibulum at eros</a>
              <a href="#" class="list-group-item"><span class="badge badge-primary">38</span>Morbi leo risus</a>
            </div>
        </div>
      </div>
  	</div>
    <div class="col-xs-7">
    <div ng-include="getInclude(selectedDs)">
      
    </div>
      
    </div>
    <div class="col-xs-2">
      <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-default btn-md" ng-show="selectedDs=='trip'" ng-click="newTrip=true;refreshMap()"><i class="fa fa-edit"></i> New Trip</button>
            <button type="button" class="btn btn-default btn-md" ng-show="selectedDs=='post'" ng-click="newPost=true"><i class="fa fa-map-marker"></i> New Post</button>
          
        </div>
      </div>
    </div>
  </div>
  </div>
</body>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyB3kvElTnywU1VxfiaRu76CNjYdknbRBL0&sensor=false">
    </script>
<script type="text/javascript" src="/assets/dashboard.js"></script>
<script type="text/javascript">
  angular.module('traflr-dashboard')
  .constant('traflrInit',{
    token: '<? echo $this->session->userdata('token'); ?>'
  });
  var subLoc = {};
  var located = [];
</script>
</html>