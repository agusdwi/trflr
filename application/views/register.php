<!DOCTYPE html>
<html ng-app="traflr-home">
<head>
	<title>Traflr</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/home.css">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

  <?=$this->load->view('include/token');?>
  
</head>
<body>
  <nav class="navbar navbar-default navbar-static-top trflr-navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><span class="trflr-brand-t">T</span>raflr</a>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="col-xs-3"></div>
      <div class="col-xs-6 well" style='height:400px;margin-top:100px'>
        <?php if($this->session->flashdata('error')){
            echo "<div class='alert alert-warning'>".$this->session->flashdata('error')."</div>";
          } ?>
        <form role="form" method="post" action="<?php echo base_url('auth/register');?>">
          <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="Username">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-default pull-right">Submit</button>
        </form>
      </div>
      <div class="col-xs-3"></div>
    </div>
  </div>
  
</body>
<script type="text/javascript" src="<?=base_url()?>/assets/home.js"></script>
</html>