<!DOCTYPE html>
<html ng-app="traflr">
<head>
	<title><? echo $user['username']; ?> | Traflr</title>
	<link rel="stylesheet" type="text/css" href="/assets/style.css">
</head>
<body>
  <nav class="navbar navbar-default navbar-static-top trflr-navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<? echo base_url('blog/'.$user["username"].''); ?>"><span class="trflr-brand-t">T</span>raflr</a>
      </div>
    </div>
  </nav>
  <div class="container" ng-controller="BlogCtrl">
  	<div class="row">
  	<div class="col-xs-4">
      <div class="row">
        <div class="col-xs-12">
          <div class="jumbotron trflr-profile-side">
            <div class="row trflr-profife-wrapper">
              <div class="col-xs-4 trflr-avatar-wrapper">
                <a href="#" class="trflr-profile-avatar">
                  <img gravatar-src="'<?echo $user['email']; ?>'" gravatar-size="100">
                </a>
              </div>
              <div class="col-xs-8 trflr-name-wrapper">
                <h5 class="trflr-profile-name"><a href="<? echo base_url('blog/'.$user["username"].''); ?>"><? echo "@".$user['username']; ?></a></h5>
              </div>
            </div>
            <div class="jumbotron-photo"><img src="/assets/images/blur-background12.jpg" /></div>
            <div class="jumbotron-contents">
              <div class="row">
                <div class="col-xs-6 trflr-label-user"><i class="fa fa-globe"></i> {{ trips.total }} trip</div>
                <div class="col-xs-6 trflr-label-user"><i class="fa fa-exchange"></i> 530 km</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
           <div class="list-group">
              <a href="#" class="list-group-item active">Cras justo odio</a>
              <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
              <a href="#" class="list-group-item">Morbi leo risus</a>
              <a href="#" class="list-group-item">Porta ac consectetur ac</a>
              <a href="#" class="list-group-item">Vestibulum at eros</a>
              <a href="#" class="list-group-item"><span class="badge badge-primary">38</span>Morbi leo risus</a>
            </div>
        </div>
      </div>
  	</div>
    <div class="col-xs-8">
      <div class="row" ng-repeat="tr in trips.data">
        <div class="jumbotron trflr-post">
          <h1 class="trflr-title"><a href="<? echo base_url('/blog/'.$user['username'].'/post');?>/{{tr.id}}"> {{ tr.title }}</a></h1>
          <div class="jumbotron-photo">
            <google-map center="{latitude:getLatitude(tr.coordinat),longitude:getLongitude(tr.coordinat)}" zoom="12" draggable="false">
               <marker coords="{latitude:getLatitude(tr.coordinat),longitude:getLongitude(tr.coordinat)}" >
                  <marker-label content="tr.title" anchor="22 0" class="marker-labels"/>
                </marker>
            </google-map>
          </div>
          <div class="jumbotron-contents">
            <div class="trflr-post-menu">
              <div class="row">
                <div class="col-xs-4">
                  <div class="row">
                    <div class="col-xs-6"><i class="fa fa-map-marker"></i> 14 Locations</div>
                    <div class="col-xs-6"><i class="fa fa-exchange"></i> 100 Km</div>
                  </div>
                </div>
                <div class="col-xs-4 pull-right">
                  <div class="row">
                    <div class="col-xs-6 pull-right" style="text-align:right"><i class="fa fa-thumbs-up"></i> Likes</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</body>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB3kvElTnywU1VxfiaRu76CNjYdknbRBL0&sensor=false">
    </script>
<script type="text/javascript" src="/assets/script.js"></script>
<script type="text/javascript">
  angular.module('traflr')
  .constant('usInit',{
    id: '<? echo $user['id']; ?>',
    mail: '<? echo $user['email']; ?>',
    uname: '<? echo $user['username']; ?>'
  });
</script>
</html>