<!DOCTYPE html>
<html ng-app="traflr-home">
<head>
	<title>Traflr</title>
	<link rel="stylesheet" type="text/css" href="<?=base_url()?>/assets/home.css">
  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">

  <?=$this->load->view('include/token');?>
  
</head>
<body>
  <nav class="navbar navbar-default navbar-static-top trflr-navbar-default" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><span class="trflr-brand-t">T</span>raflr</a>
      </div>
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
        <ul class="nav navbar-nav">
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <div ng-controller="ModalDemoCtrl">
            <script type="text/ng-template" id="loginModal.html">
                <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" ng-click="cancel()" aria-hidden="true"><i class="fa fa-times"></i></button>
                    <h4 class="modal-title">Login</h4>
                  </div>
                  <div class="modal-body">
                    <form role="form" method="post" action="auth/login">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Username or Email address</label>
                        <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Username or Enter email">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                      </div>
                      <button type="submit" class="btn btn-default pull-right" name="submit">Submit</button>
                    </form>
                  </div>
                  <div class="modal-footer">
                  </div>
                </div>
              </div>
            </script>

            <button class="btn btn-danger navbar-btn" ng-click="open()">Sign In</button>
            <div ng-show="selected">Selection from a modal: {{ selected }}</div>
          </div>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container">
    <div class="row">
      <div class="col-xs-7"></div>
      <div class="col-xs-5 well" style="margin-top:100px;background:rgba(255,255,255,0.8);">
        <div class="row">
          <div class="col-xs-12">
            <blockquote>
              <p>Register now</p>
            </blockquote>
            <form role="form" method="post" action="<?php echo base_url('auth/register');?>">
              <div class="form-group">
                <label for="exampleInputEmail1">Username</label>
                <input type="text" name="username" class="form-control" id="exampleInputEmail1" placeholder="Username">
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="text" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email Address">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
              </div>
              <button type="submit" class="btn btn-default pull-right">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  
</body>
<script type="text/javascript" src="<?=base_url()?>/assets/home.js"></script>
</html>