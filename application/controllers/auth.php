<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	public function login(){
		if (is_post()) {
			$auth = $this->user_model->auth();
			if($auth->success){
				$data['token'] = $auth->token;
				$data['email'] = $auth->user->email;
				$data['username'] = $auth->user->username;
				$this->session->set_userdata($data);
				redirect(base_url('dashboard'));
			}else{
				$this->session->set_flashdata('error', 'Username/Email or Password Invalid');
			}
		}

		if($this->session->userdata('token')){
			redirect(base_url('dashboard'));
		}else{
			$this->load->view('login');
		}
	}

	public function register(){
		if (is_post()) {
			$auth = $this->user_model->register();
			if($auth->success){
				$this->session->set_flashdata('success', 'Successfully registered, please login below.');
				redirect(base_url('auth/login'));
			}else{
				$this->session->set_flashdata('error', $auth->message);
			}
		}

		if($this->session->userdata('token')){
			redirect(base_url('dashboard'));
		}else{
			$this->load->view('register');
		}
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}
}	