<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	public function index($username){

		$cek = $this->user_model->cekUsername($username);

		if($cek->success){
			$data['user'] = $cek->data;
			$this->load->view('blog',$data);
		}else{
			redirect(base_url());
		}
	}

	public function post($username,$trip){
		$cek = $this->user_model->cekUsername($username);

		if($cek->success){
			$data['user'] = $cek->data;
			$data['trip_id'] = $trip;
			$this->load->view('post',$data);
		}else{
			redirect(base_url());
		}
	}
}