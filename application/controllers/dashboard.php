<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function index(){
		$this->load->helper('url');
		if($this->session->userdata('token')){
			$this->load->view('dashboard');
		}else{
			redirect(base_url());
		}
		
	}
}