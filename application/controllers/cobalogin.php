<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cobalogin extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}

	public function index(){

		if (is_post()) {
			$auth = $this->user_model->auth();
			if($auth->success){
				$data['token'] = $auth->token;
				$data['email'] = $auth->user->email;
				$this->session->set_userdata($data);
			}else die('salah');
		}

		$this->load->view('coba_login');
	}

	function logout(){
		$this->session->sess_destroy();
	}
}	