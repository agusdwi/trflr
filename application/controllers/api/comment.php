<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('comment_model');

		$headers 		= $_SERVER;
		$this->token 	= (isset($headers['token'])) ? $headers['token'] : '' ;
		$this->id 		= $this->user_model->authToken($this->token);
	}
	
	function trip($id,$action="page",$page=0){
		if($action=="page"){
			if (is_post()) {
				$comment_trip = $this->comment_model->insert_trip($this->id,$id);
				if ($comment_trip->success){
					json_out(array('success'=>1,'message'=>$comment_trip->message));
				}else{
					json_out(array('success'=>0,'message'=>$comment_trip->message));
				}
			}else{
				json_out($this->comment_model->get_trip($id,$page));
			}
		}else if($action == "delete"){
			$comment_delete = $this->comment_model->delete_trip($this->id,$id);
			if ($comment_delete->success){
				json_out(array('success'=>1,'message'=>$comment_delete->message));
			}else{
				json_out(array('success'=>0,'message'=>$comment_delete->message));
			}
		}
	}

	function post($id,$action="page",$page=0){
		if($action=="page"){
			if (is_post()) {
				$comment_post = $this->comment_model->insert_post($this->id,$id);
				if ($comment_post->success){
					json_out(array('success'=>1,'message'=>$comment_post->message));
				}else{
					json_out(array('success'=>0,'message'=>$comment_post->message));
				}
			}else{
				json_out($this->comment_model->get_post($id,$page));
			}
		}else if($action == "delete"){
			$comment_delete = $this->comment_model->delete_post($this->id,$id);
			if ($comment_delete->success){
				json_out(array('success'=>1,'message'=>$comment_delete->message));
			}else{
				json_out(array('success'=>0,'message'=>$comment_delete->message));
			}
		}
	}
}