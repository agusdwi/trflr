<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Feed extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('feed_model');
	}

	function index(){
		$this->trip();
	}

	function trip($page=0){
		json_out($this->feed_model->get($page));
	}

	function user($user="",$page=0){
		json_out($this->feed_model->get_user_trip($user,$page));
	}

	function trip_detail($trip,$page=0){
		json_out($this->feed_model->get_trip_detail($trip,$page));
	}

	function all_trip_detail_from_user($user,$page=0){
		json_out($this->feed_model->get_all_trip_detail_user($user,$page));
	}
}