<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('post_model');

		$headers 		= $_SERVER;
		$this->token 	= (isset($headers['HTTP_TOKEN'])) ? $headers['HTTP_TOKEN'] : '' ;
		$this->user_model->authToken($this->token);
	}

	function create(){
		if (is_post()) {
			$post = $this->post_model->insert($this->token);
			if ($post->success) {
				json_out(array('success'=>1,'message'=>$post->message,'data'=>$post->data));
			}else{
				json_out(array('success'=>0,'message'=>$post->message));
			}
		}else show_404();
	}

	function edit($id=""){
		if (is_post()) {
			$post = $this->post_model->edit($this->token,$id);
			if ($post->success) {
				json_out(array('success'=>1,'message'=>'edit successfully'));
			}else{
				json_out(array('success'=>0,'message'=>$post->message));
			}
		}else{
			$post = $this->post_model->get($this->token,$id);
			if ($post->success) {
				json_out(array('success'=>1,'data'=>$post->data));
			}else{
				json_out(array('success'=>0,'message'=>$post->message));
			}
		}
	}

	function delete($id=""){
		$post = $this->post_model->delete($this->token,$id);
		if ($post->success) {
			json_out(array('success'=>1,'message'=>'delete successfully'));
		}else{
			json_out(array('success'=>0,'message'=>$post->message));
		}
	}
}