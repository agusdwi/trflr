<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Like extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('like_model');

		$headers 		= $_SERVER;
		$this->token 	= (isset($headers['HTTP_TOKEN'])) ? $headers['HTTP_TOKEN'] : '' ;
		$this->id 		= $this->user_model->authToken($this->token);
	}
	
	function trip($id,$action="page",$page=0){
		if($action=="page"){
			if (is_post()) {
				$like_trip = $this->like_model->insert_trip($this->id,$id);
				if ($like_trip->success){
					json_out(array('success'=>1,'message'=>$like_trip->message));
				}else{
					json_out(array('success'=>0,'message'=>$like_trip->message));
				}
			}else{
				json_out($this->like_model->get_trip($id,$page));
			}
		}else if($action == "unlike"){
			$trip_unlike = $this->like_model->delete_trip($this->id,$id);
			if ($trip_unlike->success){
				json_out(array('success'=>1,'message'=>$trip_unlike->message));
			}else{
				json_out(array('success'=>0,'message'=>$trip_unlike->message));
			}
		}
	}

	function post($id,$action="page",$page=0){
		if($action=="page"){
			if (is_post()) {
				$comment_post = $this->like_model->insert_post($this->id,$id);
				if ($comment_post->success){
					json_out(array('success'=>1,'message'=>$comment_post->message));
				}else{
					json_out(array('success'=>0,'message'=>$comment_post->message));
				}
			}else{
				json_out($this->like_model->get_post($id,$page));
			}
		}else if($action == "unlike"){
			$trip_unlike = $this->like_model->delete_post($this->id,$id);
			if ($trip_unlike->success){
				json_out(array('success'=>1,'message'=>$trip_unlike->message));
			}else{
				json_out(array('success'=>0,'message'=>$trip_unlike->message));
			}
		}
	}
}