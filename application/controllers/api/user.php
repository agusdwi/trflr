<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();

		$headers 		= $_SERVER;
		$this->token 	= (isset($headers['HTTP_TOKEN'])) ? $headers['HTTP_TOKEN'] : '' ;
	}

	function register(){
		if (is_post()) {
			$reg = $this->user_model->register();
			if ($reg->success) {
				json_out(array('success'=>1,'message'=>'register successfully'));
			}else{
				json_out(array('success'=>0,'message'=>$reg->message));
			}
		}else{
			show_404();
		}
	}

	function login(){
		if (is_post()) {
			$reg = $this->user_model->auth();
			if ($reg->success) {
				json_out(array('success'=>1,'message'=>'login successfully','token'=>$reg->token));
			}else{
				json_out(array('success'=>0,'message'=>$reg->message));
			}
		}else{
			show_404();
		}
	}

	function forgot($hash=""){
		if (is_post()) {
			if ($hash == "") {
				$new = $this->user_model->generate_forgot_email();
				if($new->success){
					json_out(array('success'=>1,'message'=>$new->message));
				}else{
					json_out(array('success'=>0,'message'=>$new->message));
				}
			}else{
				$set = $this->user_model->set_new_password($hash);
				if($set->success){
					json_out(array('success'=>1,'message'=>$set->message));
				}else{
					json_out(array('success'=>0,'message'=>$set->message));
				}				
			}
		}else show_404();
	}

	// todo
	function update_profil(){}

	function password(){}
	
	function profile_picture(){}
}