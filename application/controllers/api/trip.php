<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trip extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('trip_model');

		$headers 		= $_SERVER;
		$this->token 	= (isset($headers['HTTP_TOKEN'])) ? $headers['HTTP_TOKEN'] : '' ;
		$this->user_model->authToken($this->token);
	}

	function create(){
		if (is_post()) {
			$trip = $this->trip_model->insert($this->token);
			if ($trip->success) {
				json_out(array('success'=>1,'message'=>'create trip successfully','data'=>$trip->data));
			}else{
				json_out(array('success'=>0,'message'=>$trip->message));
			}
		}else show_404();
	}

	function edit($id=""){
		if (is_post()) {
			$trip = $this->trip_model->edit($this->token,$id);
			if ($trip->success) {
				json_out(array('success'=>1,'message'=>'edit successfully'));
			}else{
				json_out(array('success'=>0,'message'=>$trip->message));
			}
		}else{
			$trip = $this->trip_model->get($this->token,$id);
			if ($trip->success) {
				json_out(array('success'=>1,'data'=>$trip->data));
			}else{
				json_out(array('success'=>0,'message'=>$trip->message));
			}
		}
	}

	function delete($id=""){
		$trip = $this->trip_model->delete($this->token,$id);
		if ($trip->success) {
			json_out(array('success'=>1,'message'=>'delete successfully'));
		}else{
			json_out(array('success'=>0,'message'=>$trip->message));
		}
	}
}